﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WAV_Scrambler.BusinessLayer;

namespace GUI_WAV_Scrambler {
    public partial class Form1 : Form {
        WAV wav;
        Scramble scramble;
        string filename = "";

        /// <summary>
        /// Default constructor
        /// </summary>
        public Form1() {
            // initialize buttons and labels
            InitializeComponent();
            labelTitleSong.Text = "Open a file";
            buttonDescramble.Enabled = false;
            buttonScramble.Enabled = false;
            buttonPlay.Enabled = false;
            buttonStop.Enabled = false;
        }

        /*****************
        * General
        ******************/

        /// <summary>
        /// Create the WAV headers
        /// </summary>
        /// <param name="header"></param>
        private void CreateHeader(string header) {
            if (header == "original" && wav != null) {
                labelHeader.Text = wav.ToString();
            } else if (header == "scrambled" && scramble != null) {
                labelHeaderDescrambled.Text = scramble.ToString();
            } else {
                labelHeaderDescrambled.Text = "Header";
                labelHeader.Text = "Header";
            }
        }

        /// <summary>
        /// Reset variables when a new song is opened
        /// </summary>
        private void Reset() {
            // reset
            wav = null;
            scramble = null;
            filename = "";
            buttonDescramble.Enabled = false;
        }

        /// <summary>
        /// Open a file
        /// </summary>
        private void Open() {
            Reset();
            OpenFileDialog open = new OpenFileDialog();
            // filter on wav files
            open.Filter = "Music file|*.wav";
            if (open.ShowDialog() == DialogResult.OK) {
                // bestand openen
                wav = new WAV(@open.FileName);
                filename = @open.FileName;
                // get the name of the music file
                string[] songName = open.FileName.Split(new char[] { '\\' });
                labelTitleSong.Text = songName[songName.Length - 1];
            }
            // create header
            CreateHeader("original");
            // enable buttons
            buttonScramble.Enabled = true;
            buttonPlay.Enabled = true;
            buttonStop.Enabled = true;
        }

        /*****************
        * Events
        ******************/


        /// <summary>
        /// Save
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveToolStripMenuItem_Click(object sender, EventArgs e) {
            // when there is no sound scrambled, you can't save
            if (scramble == null) {
                System.Windows.Forms.MessageBox.Show("You have to scramble a sound first");
            } else {
                SaveFileDialog save = new SaveFileDialog();
                save.FileName = filename;
                // filter on .wav files
                save.Filter = "Music file|*.wav";
                if (save.ShowDialog() == DialogResult.OK) {
                    scramble.Save(save.FileName);
                }
            }
        }

        private void buttonScramble_Click(object sender, EventArgs e) {
            // set the filename for the scrambled song and create it
            filename = "scrambled.wav";
            // since only stereo is supported atm, messagebox when trying to scramble mono
            try {
                scramble = new Scramble(wav);
                scramble.Play(@filename);
                // create the header
                CreateHeader("scrambled");
                // enable descramble button
                buttonDescramble.Enabled = true;
                // edit the now playing songname
                labelTitleSong.Text = filename;
            } catch (Exception err) {
                System.Windows.Forms.MessageBox.Show(err.Message);
            }
        }

        /// <summary>
        /// Descramble the song
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonDescramble_Click(object sender, EventArgs e) {
            scramble.DeScramble();
            filename = "descrambled.wav";
            scramble.Play(filename);
            labelTitleSong.Text = filename;
            buttonScramble.Enabled = false;
        }

        /// <summary>
        /// Play the song
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonPlay_Click(object sender, EventArgs e) {
            wav.Play(@filename);
        }

        /// <summary>
        /// Stop the song
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonStop_Click(object sender, EventArgs e) {
            // an empty string as parameter will result in an empty datastream so the song will stop
            wav.Stop("");
        }

        private void buttonOpen_Click(object sender, EventArgs e) {
            Open();
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e) {
            Open();
        }

        /// <summary>
        /// Exit the program
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exitToolStripMenuItem_Click(object sender, EventArgs e) {
            Environment.Exit(0);
        }

        /// <summary>
        /// About
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e) {
            AboutBox1 about = new AboutBox1();
            about.Show();
        }
    }
}