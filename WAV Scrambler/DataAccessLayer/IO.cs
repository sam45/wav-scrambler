﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;

namespace WAV_Scrambler.DataAccessLayer {
    class IO {

        /// <summary>
        /// Open a WAV file
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        internal byte[] Open(string path) {
            byte[] bytes = null;
            // open the audio data 
            try {
                bytes = File.ReadAllBytes(path);
            } catch (FileNotFoundException e) {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
            return bytes;
        }

        /// <summary>
        /// Save a WAV file
        /// </summary>
        internal void Save(string path, byte[] file) {
            try {
                File.WriteAllBytes(path, file);
            } catch (Exception e) {
                System.Diagnostics.Debug.WriteLine(e.Message);
            }
        }

        /// <summary>
        /// Play a file
        /// </summary>
        /// <param name="path"></param>
        //internal void Play(SoundPlayer player) {
        internal void Play(string path) {
            byte[] result = System.IO.File.ReadAllBytes(@path);
            System.IO.MemoryStream ms = new System.IO.MemoryStream(result);
            SoundPlayer sp = new SoundPlayer(ms);
            sp.Play();  
        }

        /// <summary>
        /// Stop a file
        /// </summary>
        /// <param name="player"></param>
        internal void Stop(string path) {
            System.IO.MemoryStream ms = new System.IO.MemoryStream();
            SoundPlayer sp = new SoundPlayer(ms);
            sp.Stop();
        }
    }
}