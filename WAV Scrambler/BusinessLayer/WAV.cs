﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WAV_Scrambler.DataAccessLayer;

namespace WAV_Scrambler.BusinessLayer {
    public class WAV : IDataLayer {
        IO io = new IO();
        byte[] bytes;
        byte[] header;
        byte[] actualData;
        string path;

        
        // required header information
        byte[] format = new byte[4];
        int channelsInt, sampleRateInt, bitsPerSampleInt;

        public int SampleRate {
            get { return sampleRateInt; }
            set { sampleRateInt = value; }
        }

        public int BitsPerSample {
            get { return bitsPerSampleInt; }
            set { bitsPerSampleInt = value; }
        }

        public int Channels {
            get { return channelsInt; }
            set { channelsInt = value; }
        }

        // properties
        public byte[] ActualData {
            get { return actualData; }
        }

        public byte[] Header {
            get { return header; }
        }

        public byte[] Bytes {
            get { return bytes; }
            set { bytes = value; }
        }

        public byte[] Format {
            get { return format; }
        }

        public string Path {
            get { return path; }
            set { path = value; }
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="path"></param>
        public WAV() {
            //this.path = @"testfileScrambled.wav"; // default song
            this.path = @"..\..\..\Content\fragment1.wav";
            bytes = Open(path);
            header = GetHeaderInformation(bytes);
            actualData = GetActualData(bytes);
        }

        /// <summary>
        /// Non default constructor
        /// </summary>
        /// <param name="path"></param>
        public WAV(string path) {
            this.path = path;
            bytes = Open(path);
            header = GetHeaderInformation(bytes);
            actualData = GetActualData(bytes);
        }

        /// <summary>
        /// Get all header information
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public byte[] GetHeaderInformation(byte[] bytes) {
            byte[] header = new byte[44];
            byte[] numChannels = new byte[2];
            byte[] sampleRate = new byte[4];
            byte[] bitsPerSample = new byte[2];

            Buffer.BlockCopy(bytes, 0, header, 0, 44);
            Buffer.BlockCopy(bytes, 8, format, 0, 4);

            // required header information
            Buffer.BlockCopy(bytes, 22, numChannels, 0, 2);
            Buffer.BlockCopy(bytes, 24, sampleRate, 0, 4);
            Buffer.BlockCopy(bytes, 34, bitsPerSample, 0, 2);

            // channels - little endian - 2 byte
            channelsInt = BitConverter.ToInt16(numChannels, 0);
            // sample rate - little endian - 4 byte 
            sampleRateInt = BitConverter.ToInt32(sampleRate, 0);
            bitsPerSampleInt = BitConverter.ToInt16(bitsPerSample, 0);

            // return all header information
            return header;
        }

        /// <summary>
        /// Get the data without the header
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public byte[] GetActualData(byte[] bytes) {
            byte[] actualData = new byte[bytes.Length - 44];
            Buffer.BlockCopy(bytes, 44, actualData, 0, bytes.Length - 44);
            return actualData;
        }

        /// <summary>
        /// Open a file
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public byte[] Open(string path) {
            return io.Open(path);
        }

        /// <summary>
        /// Save a file
        /// </summary>
        /// <param name="path"></param>
        /// <param name="file"></param>
        public void Save(string path, byte[] file) {
            io.Save(path, file);
        }

        /// <summary>
        /// Play a file
        /// </summary>
        /// <param name="player"></param>
        public void Play(string path) {
            io.Play(@path);
        }

        /// <summary>
        /// Stop the curent item playing
        /// </summary>
        /// <param name="player"></param>
        public void Stop(string path) {
            io.Stop(@path);
        }

        /// <summary>
        /// Textual representation
        /// </summary>
        /// <returns></returns>
        public override string ToString() {
            StringBuilder sb = new StringBuilder();
            sb.Append("The file format is ");
            sb.Append(Encoding.UTF8.GetString(Format));
            sb.Append("\nThere are ");
            sb.Append(Channels);
            sb.Append(" channels");
            sb.Append("\nThe sample rate is ");
            sb.Append(SampleRate);
            sb.Append("\nThe number of bits/sample is ");
            sb.Append(BitsPerSample);
            return sb.ToString();
        }
    }
}
