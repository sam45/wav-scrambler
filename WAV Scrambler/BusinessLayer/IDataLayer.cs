﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WAV_Scrambler.DataAccessLayer {
    internal interface IDataLayer {
        byte[] Open(string path);
        void Save(string path, byte[] file);
        void Play(string path);
        void Stop(string path);
    }
}
