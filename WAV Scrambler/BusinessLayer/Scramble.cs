﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WAV_Scrambler.BusinessLayer {
    public class Scramble {
        const int CHUNKSIZE = 16384;
        WAV wav;
        byte[] output;


        /// <summary>
        /// Default constructor
        /// </summary>
        public Scramble(WAV wav) {
            this.wav = wav;
            if (wav.Channels != 2) {
                throw new Exception("Only Stereo sound is supported at the moment");
            }
            bool isScramble = true;
            Start(isScramble);
            wav.Save("scrambled.wav", output);
        }

        public void DeScramble() {
            bool isScramble = false;
            wav = new WAV("scrambled.wav");
            Start(isScramble);
            wav.Save("descrambled.wav", output);
        }

        public void Start(bool isScramble) {
            float[] left, right, channelLeftF, channelRightF;

            // get left and right channel floats
            GetArrays(wav.ActualData, out left, out right);

            // when bool isScramble is set to true: scramble
            // calculate RFFT for each chunk of 16384 floats and for every channel
            channelLeftF = CalculateRFFT(left, isScramble);
            channelRightF = CalculateRFFT(right, isScramble);

            // calculate RFFT for each chunk of 16384 floats and for every channel
            channelLeftF = CalculateRFFTBackward(channelLeftF);
            channelRightF = CalculateRFFTBackward(channelRightF);

            // output
            byte[] outputData = GetOutput(channelLeftF, channelRightF);
            output = new byte[wav.Header.Length + outputData.Length];
            System.Buffer.BlockCopy(wav.Header, 0, output, 0, wav.Header.Length);
            System.Buffer.BlockCopy(outputData, 0, output, wav.Header.Length, outputData.Length);
        }


        public void Play(string path) {
            wav.Play(@path);
        }

        public void Stop(string path) {
            wav.Stop(@path);
        }

        public void Save(string path) {
            wav.Save(path, output);
        }

        public void Open(string path) {
            wav.Open(path);
        }

        /// <summary>
        /// Calculate the forward RFFT given the data from one channel
        /// </summary>
        public float[] CalculateRFFT(float[] channelData, bool scramble) {
            // get the total length including trailing zero's
            int totalLength = ((int)Math.Ceiling(channelData.Length / (float)CHUNKSIZE)) * CHUNKSIZE;
            // the number of zero's to add
            int diff = totalLength - channelData.Length;
            // new array which will include all data including zero's
            float[] channelDataFourier = new float[totalLength];

            for (int i = 0; i < totalLength / CHUNKSIZE; i++) {
                float[] tempArray = new float[CHUNKSIZE]; // temp

                // in the last chunk check for zeros
                if (i == (totalLength / CHUNKSIZE) - 1) {

                    // in the last array, iterate every number seperatly
                    for (int j = 0; j < CHUNKSIZE; j++) {
                        int index = i * CHUNKSIZE - 1;

                        // when the index is less than the length of the data, there is a value
                        if ((index + j) < channelData.Length) {
                            tempArray[j] = channelData[index + j];
                            // when there is no data, fill with zero's
                        } else {
                            tempArray[j] = 0;
                        }
                    }
                } else {
                    // copy a part of the data to the temporary array
                    Array.Copy(channelData, i * CHUNKSIZE, tempArray, 0, CHUNKSIZE);
                }
                // forward Reverse FFT
                Fourier.RFFT(tempArray, FourierDirection.Forward);

                // scramble from 1000 - 7000 Hz
                if (scramble) {
                    for (int d = 0; d < CHUNKSIZE; d++) {
                        if (d >= 743 && d <= 5201) {
                            tempArray[d] /= 200;
                        }
                    }
                    // descramble
                } else {
                    for (int d = 0; d < CHUNKSIZE; d++) {
                        if (d >= 743 && d <= 5201) {
                            tempArray[d] *= 200;
                        }
                    }
                }

                // copy the FFT calculated temporary array to the main array
                Array.Copy(tempArray, 0, channelDataFourier, i * CHUNKSIZE, CHUNKSIZE);
            }
            return channelDataFourier;
        }

        /// <summary>
        /// Calculate the backward RFFT given the data from one channel
        /// </summary>
        /// <param name="channelData"></param>
        /// <returns></returns>
        public float[] CalculateRFFTBackward(float[] channelData) {
            // get the total length including trailing zero's
            int totalLength = channelData.Length;

            // new array which will include all data including zero's
            float[] channelDataFourier = new float[totalLength];

            for (int i = 0; i < totalLength / CHUNKSIZE; i++) {
                float[] tempArray = new float[CHUNKSIZE]; // temp
                // copy a part of the data to the temporary array
                Array.Copy(channelData, i * CHUNKSIZE, tempArray, 0, CHUNKSIZE);
                // forward Reverse FFT
                Fourier.RFFT(tempArray, FourierDirection.Backward);
                // copy the FFT calculated temporary array to the main array
                Array.Copy(tempArray, 0, channelDataFourier, i * CHUNKSIZE, CHUNKSIZE);
            }

            // divide each value by half the chunksize
            for (int i = 0; i < channelDataFourier.Length; i++) {
                channelDataFourier[i] /= (CHUNKSIZE / 2);
            }
            return channelDataFourier;
        }

        /// <summary>
        /// convert two bytes to one double in the range -1 to 1
        /// @see http://stackoverflow.com/questions/8754111/how-to-read-the-data-in-a-wav-file-to-an-array
        /// </summary>
        /// <param name="firstByte"></param>
        /// <param name="secondByte"></param>
        /// <returns></returns>
        public float bytesToFloat(byte firstByte, byte secondByte) {
            // convert two bytes to one short (little endian)
            short s = (short)(secondByte << 8 | firstByte);
            // convert to range from -1 to (just below) 1
            return (float)(s);
        }

        /// <summary>
        /// Returns left and right float arrays. 
        /// @see http://stackoverflow.com/questions/8754111/how-to-read-the-data-in-a-wav-file-to-an-array
        /// </summary>
        /// <param name="data"></param>
        /// <param name="left"></param>
        /// <param name="right"></param>
        public void GetArrays(byte[] data, out float[] left, out float[] right) {
            int pos = 0;
            int channels = wav.Channels;

            // 4 bytes per sample (16 bit stereo)
            int samples = (data.Length) / 4;

            left = new float[samples];
            right = new float[samples];

            // Write to float array/s:

            int i = 0;
            while (pos < (data.Length - 2)) {
                left[i] = bytesToFloat(data[pos], data[pos + 1]);
                pos += 2;
                if (channels == 2) {
                    right[i] = bytesToFloat(data[pos], data[pos + 1]);
                    pos += 2;
                }
                i++;
            }
        }


        /// <summary>
        /// output, convert floatarrays to bytes and put channels back together
        /// </summary>
        /// <param name="leftChannel"></param>
        /// <param name="rightChannel"></param>
        /// <returns></returns>
        public byte[] GetOutput(float[] leftChannel, float[] rightChannel) {
            int arrayLength = leftChannel.Length * 4;
            // new array which will include all data including zero's
            byte[] byteArray = new byte[arrayLength];

            if (wav.Channels == 2) {
                for (int i = 0, k = 0; k < arrayLength / 4; i += 4, k++) {
                    byte[] temp = FloatsToBytes(leftChannel[k], rightChannel[k]);

                    for (int j = 0; j < 4; j++) {
                        byteArray[i + j] = temp[j];
                    }
                }
            }
            return byteArray;
        }

        /// <summary>
        /// Convert two floats to bytes
        /// </summary>
        /// <param name="firstFloat"></param>
        /// <param name="secondFloat"></param>
        /// <returns></returns>
        public byte[] FloatsToBytes(float firstFloat, float secondFloat) {
            // first convert to a 2 byte datatype before getting the bytes of the value
            short s1 = (short)firstFloat;
            short s2 = (short)secondFloat;

            byte[] bytes = new byte[4];
            byte[] array1 = BitConverter.GetBytes(s1);
            byte[] array2 = BitConverter.GetBytes(s2);

            // add left and right channel back together to create a stereo sound
            bytes[0] = array1[0];
            bytes[1] = array1[1];
            bytes[2] = array2[0];
            bytes[3] = array2[1];

            return bytes;
        }

        /// <summary>
        /// Textual representation
        /// </summary>
        /// <returns></returns>
        public override string ToString() {
            return wav.ToString();
        }
    }
}